require 'mysql2'
require 'csv'

db = Mysql2::Client.new(host: 'db', username: 'root')
db.query('use investnow')

Dir.glob('data/prices/*.csv').each do |file|
  csv = CSV.read(file, {headers: true, liberal_parsing: true})
  db.query("update funds set earliest_data = '#{csv[0]['Date']}' where name = '#{File.basename(file).sub(/\.csv$/, '')}'")
end
