require 'csv'
require 'httparty'
require 'json'
require 'date'

Dir.mkdir 'data' rescue nil

groups = {
  anz: {
    get_funds: -> {
      # https://customer.anz.co.nz/kiwisaver/Investment/FundInformation/Pages/HistoricUnitPriceReport.aspx
      name_map = {
        'Balanced Fund'            => 'OneAnswer Balanced Fund',
        'Australian Share Fund'    => 'OneAnswer Australian Share Fund',
        'Equity Selection Fund'    => 'OneAnswer Equity Selection Fund',
        'International Share Fund' => 'OneAnswer International Share Fund',
        'Property Securities Fund' => 'OneAnswer Property Securities Fund',
        'New Zealand Share Fund'   => 'OneAnswer New Zealand Share Fund',
      }
      ret = []
      Dir.glob('data/anz/*.csv').each do |file|
        CSV.open(file, 'r:bom|utf-8', {headers: true, liberal_parsing: true}) do |csv|
          csv.each do |row|
            next if !row['FundExitValue']
            fund_name = name_map[row['FundDescription']]
            next if !fund_name
            date = Date.parse(row['ValueDate']).strftime('%Y-%m-%d')
            price = row['FundExitValue'].sub(/\$/, '').to_f
            next if price == 0
            ret << [fund_name, date, price]
          end
        end
      end
      ret
    },
  },
  # pathfinder: {
  #   get_funds: -> {
  #     ret = []
  #     CSV.open('data/pathfinder/Pathfinder unit price history.csv', 'r:bom|utf-8', {headers: true, liberal_parsing: true}) do |csv|
  #       csv.each do |row|
  #         next if row['Name'] == 'Pathfinder World Equity Fund'
  #         next if row['Name'] == 'Pathfinder Responsible Investment Fund'
  #         next if row['Name'] == 'Pathfinder World Equity (Hedged) Fund'
  #         if row['Name'] == 'Pathfinder Commodity Plus'
  #           row['Name'] = 'Pathfinder Commodity Plus Fund'
  #         end
  #         date = row['Date'].sub(/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/, '\3-\2-\1')
  #         price = row['Unit_Price'].to_f
  #         next if price == 0
  #         ret << [row['Name'], date, price]
  #       end
  #     end
  #     ret
  #   },
  # },
  amp: {
    get_funds: -> {
      today = Date.today.strftime('%Y-%m-%d')
      funds = {
        # https://www.ampcapital.com/nz/en/pricing-and-performance
        'AMP Capital All Country Global Shares Index Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFPI_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Australasian Property Index Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFP_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Core Global Shares Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFCI_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Core Hedged Global Shares Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFHI_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Emerging Markets Shares Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFEM_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Global Listed Infrastructure Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=ZAIGI_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Global Multi-Asset Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIMA_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Global Property Securities Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFGP_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Global Shares Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFI_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Hedged Global Fixed Interest Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFQ_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Hedged Global Fixed Interest Index Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFPQ_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital Income Generator Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIGF_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital NZ Cash Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFD_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital NZ Fixed Interest Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFF_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital NZ Shares Index Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFPE_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital RIL Balanced Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFRI_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital RIL Conservative Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFN_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital RIL Global Shares Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFR_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital RIL Growth Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFG_X&dateFrom=1990-01-01&dateTo=#{today}",
        'AMP Capital RIL NZ Shares Fund' => "https://www.ampcapital.com/nz/en/pricing-and-performance/_jcr_content/par-content/price_performance_re_529941250/par-content-price-performance/price_performance_re_71350208.historical-data-csv.ssirequest?productCode=AIFRE_X&dateFrom=1990-01-01&dateTo=#{today}",
      }
      ret = []
      funds.each do |fund_name, url|
        puts fund_name
        csv = HTTParty.get(url).body
        # The first five lines are surplus to requirements
        5.times { csv.sub!(/.*\n/, '') }
        CSV.parse(csv, {headers: true, liberal_parsing: true}).map do |row|
          date = row['Effective Date'].sub(/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/, '\3-\2-\1')
          price = row['Buy'].to_f
          next if price == 0
          ret << [fund_name, date, price]
        end
      end
      ret
    },
  },
  nikko: {
    get_funds: -> {
      funds = {
        'Nikko AM Concentrated Equity Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0600&span=20y',
        'Nikko AM Core Equity Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0621&span=20y',
        'Nikko AM Global Bond Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0625&span=20y',
        'Nikko AM Global Equity Hedged Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0627&span=20y',
        'Nikko AM Global Equity Unhedged Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0626&span=20y',
        'Nikko AM Global Shares Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0633&span=20y',
        'Nikko AM Income Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0602&span=20y',
        'Nikko AM NZ Bond Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0623&span=20y',
        'Nikko AM NZ Cash Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0604&span=20y',
        'Nikko AM NZ Corporate Bond Fund' => 'https://www.nikkoam.co.nz/api/fund/csv?fund_code=TW0620&span=20y',
      }
      ret = []
      funds.each do |fund_name, url|
        puts fund_name
        dists = 0
        csv = HTTParty.get(url).body
        CSV.parse(csv, {headers: true, liberal_parsing: true}).map do |row|
          date = row['Date']
          dists += row['Distribution Price'].to_f / 100.0
          price = row['Purchase Price'].to_f + dists
          next if price == 0
          ret << [fund_name, date, price]
        end
      end
      ret
    },
  },
  smartshares: {
    get_funds: -> {
      today = Date.today.strftime('%m/%d/%Y')
      funds = {
        'Smartshares Australian Mid Cap Fund (MZY)' => "https://quotes.wsj.com/etf/NZ/XNZE/MZY/historical-prices/download?MOD_VIEW=page&num_rows=6299.041666666667&range_days=6299.041666666667&startDate=09/06/2000&endDate=#{today}",
        'Smartshares Australian Top 20 Fund (OZY)' => "https://quotes.wsj.com/etf/NZ/XNZE/OZY/historical-prices/download?MOD_VIEW=page&num_rows=6299.041666666667&range_days=6299.041666666667&startDate=09/06/2000&endDate=#{today}",
        'Smartshares Europe Fund (EUF)' => "https://quotes.wsj.com/NZ/XNZE/EUF/historical-prices/download?MOD_VIEW=page&num_rows=6299.041666666667&range_days=6299.041666666667&startDate=09/06/2000&endDate=#{today}",
        'Smartshares NZ Mid Cap Fund (MDZ)' => "https://quotes.wsj.com/etf/NZ/XNZE/MDZ/historical-prices/download?MOD_VIEW=page&num_rows=6299.041666666667&range_days=6299.041666666667&startDate=09/06/2000&endDate=#{today}",
        'Smartshares NZ Top 50 Fund (FNZ)' => "https://quotes.wsj.com/etf/NZ/XNZE/FNZ/historical-prices/download?MOD_VIEW=page&num_rows=6299.041666666667&range_days=6299.041666666667&startDate=09/06/2000&endDate=#{today}",
        'Smartshares Total World Fund (TWF)' => "https://quotes.wsj.com/NZ/XNZE/TWF/historical-prices/download?MOD_VIEW=page&num_rows=6299.041666666667&range_days=6299.041666666667&startDate=09/06/2000&endDate=#{today}",
        'Smartshares US 500 Fund (USF)' => "https://quotes.wsj.com/NZ/XNZE/USF/historical-prices/download?MOD_VIEW=page&num_rows=6299.041666666667&range_days=6299.041666666667&startDate=09/06/2000&endDate=#{today}",
      }
      ret = []
      funds.each do |fund_name, url|
        puts fund_name
        csv = HTTParty.get(url).body
        csv.gsub!(/, /, ',')
        CSV.parse(csv, {headers: true, liberal_parsing: true}).map do |row|
          date = row['Date'].sub(/^(\d\d)\/(\d\d)\/(\d\d)$/, '20\3-\1-\2')
          price = row['Close'].to_f
          ret << [fund_name, date, price]
        end
      end
      ret
    },
  },
  vanguard: {
    get_funds: -> {
      funds = {
        'Vanguard International Shares Select Exclusions Index Fund' => 'https://intlgra-graapp-94-prd.gra.international.vgdynamic.info/rs/gre/gra/datasets/auw-retail-price-history-mf.json?vars=portId:8122,issueType:S,sDate:2016-12-13,eDate:2018-09-11,as_of:DAILY',
        'Vanguard International Shares Select Exclusions Index Fund (Hedged) - NZD Class' => 'https://intlgra-graapp-94-prd.gra.international.vgdynamic.info/rs/gre/gra/datasets/auw-retail-price-history-mf.json?vars=portId:8125,issueType:S,sDate:2016-12-13,eDate:2018-09-11,as_of:DAILY',
      }
      ret = []
      funds.each do |fund_name, url|
        puts fund_name
        json = HTTParty.get(url).body
        data = JSON.parse(json)
        data['fundPricesNav'].each do |row|
          date = Date.parse(row['asOfDate']).strftime('%Y-%m-%d')
          price = row['price'].to_f
          ret << [fund_name, date, price]
        end
      end
      ret
    }
  },
}

fund_names = {}
all_days = {}

groups.each do |provider_name, info|
  puts provider_name

  info[:get_funds].call.each do |day|
    fund_name = day[0]
    fund_names[fund_name] = 1
    if !all_days[day[1]]
      all_days[day[1]] = {day: day[1], prices: {}}
    end
    all_days[day[1]][:prices][fund_name] = day[2]
  end
end

all_day_keys = all_days.keys.sort

# All in one file
# csv_string = CSV.generate do |csv|
#   csv << ['Date'] + fund_names.keys
#   all_day_keys.each do |day_key|
#     row = [day_key]
#     fund_names.keys.each do |fund_name|
#       price = all_days[day_key][:prices][fund_name]
#       row << price || ''
#     end
#     csv << row
#   end
# end

# IO.write('funds.csv', csv_string)

# Separate files
fund_names.keys.each do |fund_name|
  csv_string = CSV.generate do |csv|
    csv << ['Date', 'Price']
    all_day_keys.each do |day_key|
      fund_names.keys.each do |day_fund_name|
        next if day_fund_name != fund_name
        price = all_days[day_key][:prices][fund_name]
        next if !price
        csv << [day_key, price]
      end
    end
  end
  IO.write("data/prices/#{fund_name}.csv", csv_string)
end
