require 'sinatra/base'
require 'json'
require 'mysql2'
require 'csv'
require 'digest'

SOCKET_PATH = File.expand_path('/tmp/sinatra.sock')

class App < Sinatra::Base
  set :server, 'thin'
  set :bind, SOCKET_PATH

  helpers do
    def json
      headers['Content-Type'] = 'application/json'
    end

    def db
      return @db if @db

      @db = Mysql2::Client.new(host: 'db', username: 'root')
      @db.query('use investnow')
      @db
    end
  end

  get '/api/providers' do
    json
    db.query('SELECT * FROM providers ORDER BY name').to_a.to_json
  end

  get '/api/funds' do
    json

    res = db.query('SELECT * FROM funds ORDER BY provider, name')

    # DECIMAL columns come out in scientific notation, ie 0.38e0, which is not helpful.
    res.each do |row|
      row.each do |k, v|
        row[k] = v.to_f if v.is_a?(BigDecimal)
      end
    end

    res.to_a.to_json
  end

  post '/api/optimise' do
    json

    body = JSON.parse(request.body.read)
    selected_funds = body['funds']

    funds = db.query('SELECT * FROM funds ORDER BY provider, name').to_a
    funds.delete_if { |f| !selected_funds.include?(f['name']) }

    nonce = Digest::MD5.hexdigest(Random.rand().to_s)

    options = {
      funds: funds
        .keep_if { |f| File.exist?("data/prices/#{f['name']}.csv") }
        .map { |f| f['name'] },
      nonce: nonce,
    }

    cmd = "Rscript portfolio.R '#{JSON.dump(options)}'"
    puts cmd
    puts `#{cmd}`

    {
      points: CSV.read("data/output/annualizedPoints-#{nonce}.csv", {headers: true}).to_a,
      allocations: CSV.read("data/output/allocations-#{nonce}.csv", {headers: true}).to_a,
      nonce: nonce,
    }.to_json
  end
end

App.run!
