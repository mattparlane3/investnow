require 'json'
require 'mysql2'
require 'nokogiri'
require 'csv'

db = Mysql2::Client.new(host: 'db', username: 'root')
db.query('use investnow')

def fix_chars(s)
  s
    .gsub(/\u00A0/, ' ')
    .gsub('–', '-')
    .gsub(/\u00D0/, '-')
    .gsub(/\u00CA/, ' ')
    .gsub(/\u00E2\u0080\u0093/, ' ') # this one is an invisible space
end

# Get the provider name from a fund name
def provider_from_fund(fund_name)
  {
    # Substring which uniquely identifies a provider => full provider name
    'AMP Capital' => 'AMP Capital',
    'Devon ' => 'Devon Funds Management',
    'Elevation Capital' => 'Elevation Capital',
    'Fisher Funds' => 'Fisher Funds',
    'Harbour' => 'Harbour Asset Management',
    'Hunter' => 'Hunter Investment Management',
    'Legg Mason' => 'Legg Mason Global Asset Management',
    'Mint ' => 'Mint Asset Management',
    'Morphic ' => 'Morphic Asset Management',
    'Nikko AM ' => 'Nikko Asset Management',
    'OneAnswer ' => 'ANZ Investments/OneAnswer',
    'Pathfinder' => 'Pathfinder Asset Management',
    'Russell ' => 'Russell Investments',
    'Salt ' => 'Salt Funds Management',
    'Smartshares' => 'Smartshares',
    'Rowe Price' => 'T. Rowe Price (provided by Harbour Asset Management)',
    'Vanguard' => 'Vanguard',
    'India Avenue' => 'India Avenue',
    'Antipodes Global' => 'Antipodes Global',
    'APN ' => 'APN Property Group',
    'Milford ' => 'Milford Asset Management',
    'Pie ' => 'Pie Funds',
    'Platinum ' => 'Platinum Asset Management',
    'QuayStreet' => 'QuayStreet',
  }.each do |substring, provider|
    return provider if fund_name.match?(substring)
  end
  nil
end

funds = {}

doc = Nokogiri::HTML(fix_chars(IO.read('data/table.html', encoding: 'UTF-8')))
doc.css('tbody tr').each do |tr|
  cells = tr.css('td')
  fund_name = fix_chars(cells[2].text)
  next if !fund_name

  fees = cells[3].text.to_f
  fact_sheet = nil
  pds = nil

  cells[6].css('a').each do |a|
    case a['ng-if']
    when 'product.factSheetUrl'
      fact_sheet = a['href'].strip
    when 'product.productDisclosureMyDocumentId'
      pds = a['href'].strip
    end
  end

  funds[fund_name] = {
    fund_name: fund_name,
    fees: fees,
    pds: pds,
    fact_sheet: fact_sheet,
  }
end

csv = CSV.read('extra.csv', {headers: true, encoding: 'UTF-8'})
csv.each do |row|
  fund_name = fix_chars(row['Fund'])
  next if !fund_name

  funds[fund_name] ||= {}

  funds[fund_name][:risk] = row['Risk'].nil? ? nil : row['Risk'].to_i
  funds[fund_name][:fees_in] = row['In'].to_f
  funds[fund_name][:fees_out] = row['Out'].to_f
end

Dir.glob('data/*.csv').sort.each do |file|
  # There's a few odd spaces in the data.
  IO.write(file, fix_chars(IO.read(file, encoding: 'UTF-8')))

  csv = CSV.read(file, {headers: true, liberal_parsing: true, encoding: 'UTF-8'})
  csv.each do |row|
    next if !row['Name'] || row['Name'] == ''
    row.delete(' ') if row[' ']

    fund_name = row['Name']
    next if !fund_name || !funds[fund_name]
    row.to_h.keys.each do |k|
      # Convert percentages to real numbers, makes sorting easier.
      row[k] = row[k].to_f if row[k] =~ /^[-\d\.]+%$/
      row[k] = nil if row[k] == '-'
      funds[fund_name][k.downcase.gsub(/\s+/, '_',).to_sym] = row[k]
    end
  end
end

funds.each do |k, fund|
  provider_name = provider_from_fund(fund[:fund_name])
  if !provider_name
    puts "No provider for #{fund[:fund_name]}"
    next
  end

  s = db.prepare("REPLACE INTO providers (name,image) VALUES (?,?)")
  r = s.execute(provider_name, '')

  fund[:risk] = fund[:risk].to_i if fund[:risk]

  %i{fees fees_in fees_out 1_month 3_months 6_months 1_year 3_years 5_years 10_years}.each do |f|
    fund[f] = fund[f].to_f if fund[f]
  end

  s = db.prepare("
    INSERT INTO funds (
      provider,name,sector,risk,fees,fees_in,fees_out,perf_1m,perf_3m,perf_6m,perf_1y,perf_3y,perf_5y,perf_10y,
      fact_sheet,pds
    )
    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
    ON DUPLICATE KEY UPDATE
      sector = ?, risk = ?, fees = ?, fees_in = ?, fees_out = ?, perf_1m = ?, perf_3m = ?, perf_6m = ?, perf_1y = ?,
      perf_3y = ?, perf_5y = ?, perf_10y = ?, fact_sheet = ?, pds = ?
  ")
  r = s.execute(provider_name, fund[:fund_name], fund[:category], fund[:risk], fund[:fees], fund[:fees_in],
    fund[:fees_out], fund[:'1_month'], fund[:'3_months'], fund[:'6_months'], fund[:'1_year'],
    fund[:'3_years'], fund[:'5_years'], fund[:'10_years'], fund[:fact_sheet], fund[:pds],

    fund[:category], fund[:risk], fund[:fees], fund[:fees_in], fund[:fees_out], fund[:'1_month'], fund[:'3_months'],
    fund[:'6_months'], fund[:'1_year'], fund[:'3_years'], fund[:'5_years'], fund[:'10_years'],
    fund[:fact_sheet], fund[:pds]
  )
end
