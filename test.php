<?php
$html = file_get_contents('html.html');
preg_match_all('/images\/funds\/(\d+)\.png/', $html, $matches);
$ids = [];
foreach ($matches[1] as $id)
	$ids[$id] = 1;
$done = [];
foreach (array_keys($ids) as $id) {
	echo $id, "\n";
	$data = file_get_contents("https://secure.investnow.co.nz/images/funds/{$id}.png");
	$md5 = md5($data);
	if (isset($done[$md5])) continue;
	$done[$md5] = 1;
	file_put_contents("thumbs/{$id}.png", $data);
}
